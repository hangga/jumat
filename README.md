# fridaytablegenerator
Berawal dari malas. Malas bolak-balik kalender, malas bolak-balik ngetik. Akhirnya iseng-iseng bikin tool ecek-ecek namun sangat berguna. Terutama untuk menjalankan tugas menyusun jadwal khatib Jum'at.
# tampilan
![alt text](https://raw.githubusercontent.com/hangga/fridaytablegenerator/master/ikih-dab.png)
# LIVE DEMO
[Link menuju tekape](https://hangga.github.io/jumat/)
